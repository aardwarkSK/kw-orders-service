# Kafka workshop - Orders service


## Quick Start

To build and start the project:

1. Build: `mvn clean package`
2. Ensure your database is started
3. Main class is located in `orders-rest` module. 
4. In `orders-rest` module run: `mvn spring-boot:run`
5. Check Actuator endpoint at `http://localhost:8082/actuator`

Also see the Postman scripts stored within the [postman](./postman) directory.


## Database


Pull PostgreSQL image from a registry.
```bash
$ docker pull postgres
```

Start container with PostgreSQL database.
```bash
$ docker run --name postgres-docker -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres
```


## Microservices used in Kafka workshop:

- **User management Microservice**
    - Responsibilities:
        - Create user
        - Update user
        - Delete user
    - Source: https://bitbucket.org/aardwarkSK/kw-user-management
    - Testing: http://localhost:xxxx
    
- **Store Microservice**
    - Responsibilities:
        - Add item
        - Remove item
    - Source: https://bitbucket.org/aardwarkSK/kw-store-service
    - Testing: http://localhost:xxxx
    
- **Item Type Microservice**
    - Responsibilities:
        - Create Item type
        - Update Item type
        - Delete Item type
    - Source: https://bitbucket.org/aardwarkSK/kw-item-type-service
    - Testing: http://localhost:xxxx
    
- **Order Microservice**
    - Responsibilities:
        - Create order
        - Get all orders or orders for specified customer
        - Get order detail
        - Change order state
            - Available states: DRAFT, OPEN, PENDING, CLOSED, CANCELED
        - Delete order
    - Source: _this repository_
    - Testing: http://localhost:8082