package com.aardwark.kworders.rest.dto;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@Builder
public class OrderItemResponse {

    @NotNull
    UUID id;

    @NotNull
    UUID itemTypeId;

    @NotNull
    Integer quantity;
}
