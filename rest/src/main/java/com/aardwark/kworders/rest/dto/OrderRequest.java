package com.aardwark.kworders.rest.dto;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Value
@Builder
public class OrderRequest {

    @NotNull
    UUID customerId;

    @NotEmpty
    Set<OrderItemRequest> orderItems;
}
