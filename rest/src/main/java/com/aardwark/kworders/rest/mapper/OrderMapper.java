package com.aardwark.kworders.rest.mapper;

import com.aardwark.kworders.resource.domain.Order;
import com.aardwark.kworders.rest.dto.OrderResponse;
import com.aardwark.kworders.rest.dto.OrderRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    Order toDomain(OrderRequest dto);

    OrderResponse toDto(Order entity);
}
