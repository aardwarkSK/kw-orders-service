package com.aardwark.kworders.rest.dto;

import com.aardwark.kworders.core.OrderStatusEnum;
import lombok.Builder;
import lombok.Value;

import java.util.Set;
import java.util.UUID;


@Value
@Builder
public class OrderResponse {

    UUID id;

    UUID customerId;

    OrderStatusEnum orderStatus;

    Set<OrderItemResponse> orderItems;

}
