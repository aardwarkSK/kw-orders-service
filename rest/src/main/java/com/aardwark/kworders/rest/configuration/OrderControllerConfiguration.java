package com.aardwark.kworders.rest.configuration;

import com.aardwark.kworders.rest.mapper.OrderMapper;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = OrderMapper.class)
public class OrderControllerConfiguration {
}
