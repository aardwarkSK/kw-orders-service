package com.aardwark.kworders.rest.controller;

import com.aardwark.kworders.core.OrderStatusEnum;
import com.aardwark.kworders.core.error.CommonErrorEnum;
import com.aardwark.kworders.core.exception.NotFoundException;
import com.aardwark.kworders.core.exception.OrderBusinessException;
import com.aardwark.kworders.resource.domain.Order;
import com.aardwark.kworders.rest.dto.OrderResponse;
import com.aardwark.kworders.rest.dto.OrderRequest;
import com.aardwark.kworders.rest.mapper.OrderMapper;
import com.aardwark.kworders.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.EnumUtils;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/orders", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
@Slf4j
public class OrderController {

    private final OrderService orderService;
    private final OrderMapper orderMapper;

    public OrderController(final OrderService orderService, final OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    @GetMapping
    public ResponseEntity<List<OrderResponse>> getAllOrders() {
        final List<Order> orders = orderService.getAllOrders();
        log.debug("Get all orders {}", orders);
        final List<OrderResponse> orderResponses = new ArrayList<>();
        for (Order order : orders) {
            orderResponses.add(orderMapper.toDto(order));
        }
        return new ResponseEntity<>(orderResponses, OK);
    }

    @GetMapping(value = "/customers/{customerId}")
    public ResponseEntity<List<OrderResponse>> getAllCustomerOrders(@PathVariable final UUID customerId) {
        final List<Order> orders = orderService.getOrdersByCustomerId(customerId);
        log.debug("Get orders {} for customer with id {}", orders, customerId);

        if (orders.isEmpty()) {
            log.error("No orders of customer with id {}", customerId);
            throw new NotFoundException(CommonErrorEnum.NOT_FOUND, "Order was not found");
        }

        final List<OrderResponse> orderResponses = new ArrayList<>();
        for (Order order : orders) {
            orderResponses.add(orderMapper.toDto(order));
        }
        return new ResponseEntity<>(orderResponses, OK);
    }

    @GetMapping(value = "/{orderId}")
    public ResponseEntity<OrderResponse> getOrderById(@PathVariable final UUID orderId) {
        final Optional<Order> order = orderService.findOrderById(orderId);
        if (order.isPresent()) {
            log.debug("Get order {} ", order.get());
            final OrderResponse orderResponse = orderMapper.toDto(order.get());
            return new ResponseEntity<>(orderResponse, OK);
        } else {
            log.error("Order with id {} was not found", orderId);
            throw new NotFoundException(CommonErrorEnum.NOT_FOUND, "Order was not found");
        }
    }

    @PostMapping
    public ResponseEntity<String> createOrder(@Valid @RequestBody final OrderRequest orderRequest) {
        final Order order = orderMapper.toDomain(orderRequest);
        final UUID newId = orderService.addOrder(order);
        final JSONObject responseId = new JSONObject()
                    .put("orderId", newId);
        return new ResponseEntity<>(responseId.toString(), CREATED);
    }

    @PutMapping(value = "/{orderId}")
    public ResponseEntity<OrderResponse> updateOrderStatus(
            @PathVariable final UUID orderId,
            @RequestParam final String orderStatus) {

        boolean isValidEnum = EnumUtils.isValidEnum(OrderStatusEnum.class, orderStatus);
        if (!isValidEnum){
            log.debug("Update of order status with id {} failed because od invalid value {}. " +
                            "Please insert one of the valid values: {}", orderId, orderStatus, OrderStatusEnum.values());
            log.error("Order status {} is not valid", orderStatus);
            throw new OrderBusinessException(CommonErrorEnum.INVALID_ORDER_STATUS, "Order status is not valid");
        }
        final Order order = orderService.updateOrderState(orderId, OrderStatusEnum.valueOf(orderStatus));
        final OrderResponse orderResponse = orderMapper.toDto(order);
        return new ResponseEntity<>(orderResponse, OK);
    }

    @DeleteMapping(value = "/{orderId}")
    public ResponseEntity<Object> deleteOrder(@PathVariable final UUID orderId) {
        orderService.deleteOrder(orderId);
        return new ResponseEntity<>(NO_CONTENT);
    }
}
