package com.aardwark.kworders.rest.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.*;

import com.aardwark.kworders.core.OrderStatusEnum;
import com.aardwark.kworders.core.exception.NotFoundException;
import com.aardwark.kworders.core.exception.OrderBusinessException;
import com.aardwark.kworders.resource.domain.Order;
import com.aardwark.kworders.resource.domain.OrderItem;
import com.aardwark.kworders.rest.dto.OrderItemRequest;
import com.aardwark.kworders.rest.dto.OrderItemResponse;
import com.aardwark.kworders.rest.dto.OrderRequest;
import com.aardwark.kworders.rest.dto.OrderResponse;
import com.aardwark.kworders.rest.mapper.OrderMapper;
import com.aardwark.kworders.service.OrderService;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
class OrderControllerTest {

    @Mock
    private OrderService orderService;

    @Mock
    private OrderMapper orderMapper;

    @InjectMocks
    private OrderController orderController;

    private static final OrderResponse ORDER_RESPONSE = createOrderResponse();
    private static final OrderRequest ORDER_REQUEST = createOrderRequest();
    private static final Order ORDER = createOrder();

    @Test
    void getAllOrders_test() {
        //GIVEN
        when(orderService.getAllOrders()).thenReturn(List.of(ORDER));
        when(orderMapper.toDto(any())).thenReturn(ORDER_RESPONSE);
        //WHEN
        final ResponseEntity<?> orders = orderController.getAllOrders();
        //THEN
        assertThat(orders.getStatusCode()).isEqualTo(OK);
        assertThat(orders.getBody()).isEqualTo(List.of(ORDER_RESPONSE));
        verify(orderService).getAllOrders();
        verify(orderMapper).toDto(ORDER);
    }

    @Test
    void getAllCustomerOrders_test() {
        final UUID customerId = UUID.randomUUID();
        //GIVEN
        when(orderService.getOrdersByCustomerId(customerId)).thenReturn(List.of(ORDER));
        when(orderMapper.toDto(any())).thenReturn(ORDER_RESPONSE);
        //WHEN
        final ResponseEntity<?> orders = orderController.getAllCustomerOrders(customerId);
        //THEN
        assertThat(orders.getStatusCode()).isEqualTo(OK);
        assertThat(orders.getBody()).isEqualTo(List.of(ORDER_RESPONSE));
        verify(orderService).getOrdersByCustomerId(customerId);
        verify(orderMapper).toDto(ORDER);
    }

    @Test
    void getOrderById_test() {
        //GIVEN
        when(orderService.findOrderById(ORDER.getId())).thenReturn(Optional.of(ORDER));
        when(orderMapper.toDto(any())).thenReturn(ORDER_RESPONSE);
        //WHEN
        final ResponseEntity<?> order = orderController.getOrderById(ORDER.getId());
        //THEN
        assertThat(order.getStatusCode()).isEqualTo(OK);
        assertThat(order.getBody()).isEqualTo(ORDER_RESPONSE);
        verify(orderService).findOrderById(ORDER.getId());
        verify(orderMapper).toDto(ORDER);
    }

    @Test
    void getOrderById_negativeTest() {
        final UUID unexistingId = UUID.randomUUID();

        //GIVEN
        when(orderService.findOrderById(unexistingId)).thenReturn(Optional.empty());

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
            orderController.getOrderById(unexistingId);
        });

        verify(orderService).findOrderById(unexistingId);
    }

    @Test
    void updateOrderStatus_Test() {
        final OrderItemResponse orderItemResponse = OrderItemResponse.builder()
                .id(UUID.randomUUID())
                .itemTypeId(UUID.randomUUID())
                .quantity(1)
                .build();

        final OrderResponse orderResponseForUpdate = OrderResponse.builder()
                .id(UUID.randomUUID())
                .customerId(UUID.randomUUID())
                .orderStatus(OrderStatusEnum.CANCELED)
                .orderItems(Collections.singleton(orderItemResponse))
                .build();

        //GIVEN
        when(orderService.updateOrderState(ORDER.getId(), OrderStatusEnum.CANCELED)).thenReturn(ORDER);
        when(orderMapper.toDto(ORDER)).thenReturn(orderResponseForUpdate);
        //WHEN
        final ResponseEntity<?> order = orderController.updateOrderStatus(ORDER.getId(), "CANCELED");
        //THEN
        assertThat(order.getStatusCode()).isEqualTo(OK);
        assertThat(order.getBody()).isEqualTo(orderResponseForUpdate);
        verify(orderService).updateOrderState(ORDER.getId(), OrderStatusEnum.CANCELED);
        verify(orderMapper).toDto(ORDER);
    }

    @Test
    void updateOrderStatus_invalidStatusTest() {
        assertThatExceptionOfType(OrderBusinessException.class).isThrownBy(() -> {
            orderController.updateOrderStatus(ORDER.getId(), "INVALID STATUS");
        });
    }


    @Test
    void createOrder_Test() {
        final JSONObject responseId = new JSONObject();
        responseId.put("orderId", ORDER.getId());

        //GIVEN
        when(orderService.addOrder(ORDER)).thenReturn(ORDER.getId());
        when(orderMapper.toDomain(ORDER_REQUEST)).thenReturn(ORDER);
        //WHEN
        final ResponseEntity<?> order = orderController.createOrder(ORDER_REQUEST);
        //THEN
        assertThat(order.getStatusCode()).isEqualTo(CREATED);
        assertThat(order.getBody()).isEqualTo(responseId.toString());
        verify(orderService).addOrder(ORDER);
        verify(orderMapper).toDomain(ORDER_REQUEST);
    }

    @Test
    void deleteOrder_Test() {
        //GIVEN
        final UUID orderId = UUID.randomUUID();
        //WHEN
        final ResponseEntity<?> orders = orderController.deleteOrder(orderId);
        //THEN
        assertThat(orders.getStatusCode()).isEqualTo(NO_CONTENT);
        verify(orderService).deleteOrder(orderId);
    }

    private static OrderResponse createOrderResponse() {
        final OrderItemResponse orderItemResponse = OrderItemResponse.builder()
                .id(UUID.randomUUID())
                .itemTypeId(UUID.randomUUID())
                .quantity(1)
                .build();

        return OrderResponse.builder()
                .id(UUID.randomUUID())
                .customerId(UUID.randomUUID())
                .orderStatus(OrderStatusEnum.DRAFT)
                .orderItems(Collections.singleton(orderItemResponse))
                .build();
    }

    private static OrderRequest createOrderRequest() {
        final OrderItemRequest orderItemRequest = OrderItemRequest.builder()
                .itemTypeId(UUID.randomUUID())
                .quantity(1)
                .build();

        return OrderRequest.builder()
                .customerId(UUID.randomUUID())
                .orderItems(Collections.singleton(orderItemRequest))
                .build();
    }


    private static Order createOrder() {
        final OrderItem orderItem = OrderItem.builder()
                .id(UUID.randomUUID())
                .itemTypeId(UUID.randomUUID())
                .quantity(1)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        return Order.builder()
                .id(UUID.randomUUID())
                .orderStatus(OrderStatusEnum.DRAFT)
                .orderItems(Collections.singleton(orderItem))
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
    }
}
