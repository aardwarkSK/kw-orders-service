package com.aardwark.kworders.core.handler;

import static com.aardwark.kworders.core.error.CommonErrorEnum.COMMON_ERROR_CODE_PREFIX;
import static java.time.LocalDateTime.now;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
@Slf4j
public class RestErrorHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            final Exception exception,
            final Object body,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request) {

        final String errorCode = COMMON_ERROR_CODE_PREFIX + status.name().toLowerCase(Locale.ENGLISH).replace('_', '.');
        final OrderErrorResponse ordersErrorResponse = new OrderErrorResponse(exception.getMessage(), errorCode, now());
        log.error("Exception occurred {}", ordersErrorResponse, exception);
        return new ResponseEntity<>(ordersErrorResponse, status);
    }
}
