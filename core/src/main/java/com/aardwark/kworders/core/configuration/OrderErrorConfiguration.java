package com.aardwark.kworders.core.configuration;

import com.aardwark.kworders.core.handler.OrderExceptionHandler;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = OrderExceptionHandler.class)
public class OrderErrorConfiguration {
}
