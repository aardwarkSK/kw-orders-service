package com.aardwark.kworders.core;

public enum OrderStatusEnum {
    DRAFT,
    OPEN,
    PENDING,
    CLOSED,
    CANCELED
}
