package com.aardwark.kworders.core.error;

import java.util.Locale;

/**
 * Common errors, not related to any specific part.
 */
public enum CommonErrorEnum implements OrderErrorCode {

    INVALID_ORDER_STATUS("Invalid order status"),
    NOT_FOUND("Data not found");

    public static final String COMMON_ERROR_CODE_PREFIX = "orders.error.common.";

    private final String code;
    private final String description;

    CommonErrorEnum(final String description) {
        this.code = COMMON_ERROR_CODE_PREFIX + name().toLowerCase(Locale.ENGLISH).replace('_', '.');
        this.description = description;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
