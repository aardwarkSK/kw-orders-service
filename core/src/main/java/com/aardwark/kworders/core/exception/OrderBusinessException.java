package com.aardwark.kworders.core.exception;

import com.aardwark.kworders.core.error.OrderErrorCode;

/**
 * Orders application business exception. Caused by wrong data input.
 */
public class OrderBusinessException extends OrderException {

    public OrderBusinessException(final OrderErrorCode error) {
        super(error);
    }

    public OrderBusinessException(final OrderErrorCode error, final String message) {
        super(error, message);
    }

    public OrderBusinessException(final OrderErrorCode error, final String message, final Throwable throwable) {
        super(error, message, throwable);
    }
}
