package com.aardwark.kworders.core.exception;

import com.aardwark.kworders.core.error.OrderErrorCode;

/**
 * Orders service system/fatal exception. Caused by internal application error.
 *
 */
public class OrderSystemException extends OrderException {

    public OrderSystemException(final OrderErrorCode error) {
        super(error);
    }

    public OrderSystemException(final OrderErrorCode error, final String message) {
        super(error, message);
    }

    public OrderSystemException(final OrderErrorCode error, final String message, final Throwable throwable) {
        super(error, message, throwable);
    }
}
