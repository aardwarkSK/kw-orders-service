package com.aardwark.kworders.core.exception;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aardwark.kworders.core.error.OrderErrorCode;

import static java.util.Objects.requireNonNull;

public class OrderException extends RuntimeException {

    private final OrderErrorCode error;

    public OrderException(final OrderErrorCode error) {
        this(error, null);
    }

    public OrderException(final OrderErrorCode error, final String message) {
        this(error, message, null);
    }

    public OrderException(final OrderErrorCode error, final String message, final Throwable throwable) {
        super(message, throwable);
        this.error = requireNonNull(error);
    }

    public OrderErrorCode getError() {
        return error;
    }

    @Override
    public String getMessage() {
        final String superMsg = super.getMessage();
        if (StringUtils.isNoneEmpty(superMsg)) {
            return superMsg;
        }
        return "[" + getError().getCode() + "] " + getError().getDescription();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("code", error.getCode())
                .append("description", error.getDescription())
                .toString();
    }
}
