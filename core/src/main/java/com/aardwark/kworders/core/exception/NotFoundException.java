package com.aardwark.kworders.core.exception;

import com.aardwark.kworders.core.error.OrderErrorCode;

/**
 * Common exception used for cases where resources or entities is not found.
 *
 */
public class NotFoundException extends OrderBusinessException {

    public NotFoundException(final OrderErrorCode error, final String message) {
        super(error, message);
    }
}
