package com.aardwark.kworders.core.handler;

import static java.time.LocalDateTime.now;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import com.aardwark.kworders.core.exception.NotFoundException;
import com.aardwark.kworders.core.exception.OrderBusinessException;
import com.aardwark.kworders.core.exception.OrderException;
import com.aardwark.kworders.core.exception.OrderSystemException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class OrderExceptionHandler extends RestErrorHandler {

    /**
     * Map {@link OrderBusinessException} to object.
     *
     * @param orderBusinessException Orders service logic exception
     * @return mapped exception to object with HTTP status 400.
     */
    @ExceptionHandler(OrderBusinessException.class)
    public ResponseEntity<OrderErrorResponse> handleOrdersLogicException(final OrderBusinessException orderBusinessException){
        final OrderErrorResponse ordersErrorResponse = handleException(orderBusinessException);
        return new ResponseEntity<>(ordersErrorResponse, BAD_REQUEST);
    }

    /**
     * Map {@link NotFoundException} to object.
     *
     * @param notFoundException not found exception
     * @return mapped exception to object with HTTP status 404.
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<OrderErrorResponse> handleOrdersFoundException(final NotFoundException notFoundException){
        final OrderErrorResponse ordersErrorResponse = handleException(notFoundException);
        return new ResponseEntity<>(ordersErrorResponse, NOT_FOUND);
    }

    /**
     * Map {@link OrderSystemException} to object.
     *
     * @param ordersSystemException Orders system exception
     * @return mapped exception to object with HTTP status 500.
     */
    @ExceptionHandler(OrderSystemException.class)
    public ResponseEntity<OrderErrorResponse> handleOrdersSystemException(final OrderSystemException ordersSystemException){
        final OrderErrorResponse ordersErrorResponse = handleException(ordersSystemException);
        return new ResponseEntity<>(ordersErrorResponse, INTERNAL_SERVER_ERROR);
    }

    private OrderErrorResponse handleException(final OrderException ordersException) {
        return new OrderErrorResponse(ordersException.getMessage(), ordersException.getError().getCode(), now());
    }
}
