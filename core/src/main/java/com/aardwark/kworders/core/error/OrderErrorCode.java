package com.aardwark.kworders.core.error;

public interface OrderErrorCode {

    String getCode();

    String getDescription();
}
