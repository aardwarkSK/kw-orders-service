CREATE TABLE orders (
    id                  UUID             PRIMARY KEY,
    created_at          timestamp        NOT NULL,
    updated_at          timestamp        NOT NULL,
    deleted_at          timestamp,
    status              order_status     NOT NULL,
    customer_id         UUID             NOT NULL
);

CREATE UNIQUE INDEX orders_ui on orders (id);
