CREATE TABLE order_item (
    id                  UUID       PRIMARY KEY,
    created_at          timestamp  NOT NULL,
    updated_at          timestamp  NOT NULL,
    deleted_at          timestamp,
    item_type_id        UUID       NOT NULL,
    quantity            bigint     NOT NULL,
    order_id            UUID,
    CONSTRAINT order_item_fk
        FOREIGN KEY (order_id)
            REFERENCES orders (id)
);

CREATE UNIQUE INDEX order_item_ui on order_item (id);
