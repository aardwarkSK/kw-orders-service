package com.aardwark.kworders.resource.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@MappedSuperclass
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public abstract class AbstractEntity {

    protected static final String ID         = "id";
    protected static final String CREATED_AT = "created_at";
    protected static final String UPDATED_AT = "updated_at";
    protected static final String DELETED_AT = "deleted_at";

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = ID, updatable = false, nullable = false)
    private UUID id;

    @Column(name = CREATED_AT, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = UPDATED_AT)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = DELETED_AT)
    private LocalDateTime deletedAt;
}
