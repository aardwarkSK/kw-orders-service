package com.aardwark.kworders.resource.domain;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "order_item")
@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE order_item SET deleted_at = now() WHERE id=?")
public class OrderItem extends AbstractEntity {

    private static final String ITEM_TYP_ID = "item_type_id";
    private static final String QUANTITY = "quantity";

    @Column(name = ITEM_TYP_ID)
    private UUID itemTypeId;

    @Column(name = QUANTITY)
    private Integer quantity;

}
