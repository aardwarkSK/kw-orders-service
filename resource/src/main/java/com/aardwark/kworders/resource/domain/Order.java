package com.aardwark.kworders.resource.domain;

import com.aardwark.kworders.core.OrderStatusEnum;
import com.aardwark.kworders.resource.EnumTypePostgreSql;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Data
@NoArgsConstructor
@TypeDef(
        name = "order_status",
        typeClass = EnumTypePostgreSql.class
)
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE orders SET deleted_at = now() WHERE id=?")
public class Order extends AbstractEntity {

    private static final String ORDER_STATUS_ID = "order_status_id";
    private static final String CUSTOMER_ID = "customer_id";
    private static final String ORDER_ID = "order_id";
    private static final String STATUS = "status";
    private static final String ORDER_STATUS = "order_status";

    @Column(name = STATUS)
    @Enumerated(EnumType.STRING)
    @Type(type = ORDER_STATUS)
    private OrderStatusEnum orderStatus;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = ORDER_ID, nullable = false)
    private Set<OrderItem> orderItems = new HashSet<>();

    @Column(name = CUSTOMER_ID)
    private UUID customerId;

}
