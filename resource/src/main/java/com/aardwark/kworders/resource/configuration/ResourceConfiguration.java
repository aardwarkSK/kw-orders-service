package com.aardwark.kworders.resource.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.aardwark.kworders.resource.repository")
@EntityScan("com.aardwark.kworders.resource.domain")
public class ResourceConfiguration {
}
