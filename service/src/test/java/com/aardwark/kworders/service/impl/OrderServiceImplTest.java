package com.aardwark.kworders.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import com.aardwark.kworders.core.OrderStatusEnum;
import com.aardwark.kworders.resource.domain.Order;
import com.aardwark.kworders.resource.domain.OrderItem;
import com.aardwark.kworders.resource.repository.OrderItemRepository;
import com.aardwark.kworders.resource.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderItemRepository orderItemRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    private static final Order ORDER = createOrder();

    @Test
    void getAllOrders_test() {
        //GIVEN
        when(orderRepository.findAll()).thenReturn(List.of(ORDER));
        //WHEN
        final List<Order> orders = orderService.getAllOrders();
        //THEN
        assertThat(orders).isEqualTo(List.of(ORDER));
        verify(orderRepository).findAll();
    }

    @Test
    void getOrdersByCustomerId_test() {
        final UUID customerId = UUID.randomUUID();

        //GIVEN
        when(orderRepository.findByCustomerId(customerId)).thenReturn(List.of(ORDER));
        //WHEN
        final List<Order> orders = orderService.getOrdersByCustomerId(customerId);
        //THEN
        assertThat(orders).isEqualTo(List.of(ORDER));
        verify(orderRepository).findByCustomerId(customerId);
    }

    @Test
    void findOrderById_test() {
        //GIVEN
        when(orderRepository.findById(ORDER.getId())).thenReturn(Optional.of(ORDER));
        //WHEN
        final Optional<Order> order = orderService.findOrderById(ORDER.getId());
        //THEN
        assertThat(order).isEqualTo(Optional.of(ORDER));
        verify(orderRepository).findById(ORDER.getId());
    }

    @Test
    void addOrder_test() {
        //GIVEN
        when(orderRepository.save(ORDER)).thenReturn(ORDER);
        //WHEN
        final UUID orderId = orderService.addOrder(ORDER);
        //THEN
        assertThat(orderId).isNotNull();
        verify(orderRepository).save(ORDER);
    }

    @Test
    void updateOrderState_test() {
        //GIVEN
        when(orderRepository.findById(ORDER.getId())).thenReturn(Optional.of(ORDER));
        when(orderRepository.save(ORDER)).thenReturn(ORDER);
        //WHEN
        final Order order = orderService.updateOrderState(ORDER.getId(), OrderStatusEnum.CANCELED);
        //THEN
        assertThat(order).isNotNull();
        assertThat(order.getOrderStatus()).isEqualTo(OrderStatusEnum.CANCELED);
        verify(orderRepository).save(ORDER);
    }

    @Test
    void deleteOrder() {
        //GIVEN
        when(orderRepository.findById(ORDER.getId())).thenReturn(Optional.of(ORDER));
        doNothing().when(orderRepository).delete(ORDER);
        final Optional<OrderItem> orderItem = ORDER.getOrderItems().stream().findFirst();
        final OrderItem orderItemEntity = orderItem.orElseThrow();
        doNothing().when(orderItemRepository).delete(orderItemEntity);
        //WHEN
        orderService.deleteOrder(ORDER.getId());
        //THEN
        verify(orderRepository).findById(ORDER.getId());
        verify(orderRepository).delete(ORDER);
        verify(orderItemRepository).delete(orderItemEntity);
    }

    private static Order createOrder() {
        final OrderItem orderItem = OrderItem.builder()
                .id(UUID.randomUUID())
                .itemTypeId(UUID.randomUUID())
                .quantity(1)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        return Order.builder()
                .id(UUID.randomUUID())
                .orderStatus(OrderStatusEnum.DRAFT)
                .orderItems(Collections.singleton(orderItem))
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
    }
}