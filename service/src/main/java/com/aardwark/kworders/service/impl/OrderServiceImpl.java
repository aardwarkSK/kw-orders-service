package com.aardwark.kworders.service.impl;

import com.aardwark.kworders.core.error.CommonErrorEnum;
import com.aardwark.kworders.core.exception.NotFoundException;
import com.aardwark.kworders.core.OrderStatusEnum;
import com.aardwark.kworders.resource.domain.OrderItem;
import com.aardwark.kworders.resource.repository.OrderItemRepository;
import com.aardwark.kworders.resource.repository.OrderRepository;
import com.aardwark.kworders.resource.domain.Order;
import com.aardwark.kworders.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;

    public OrderServiceImpl(final OrderRepository orderRepository, final OrderItemRepository orderItemRepository) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> getOrdersByCustomerId(final UUID customerId) {
        return orderRepository.findByCustomerId(customerId);
    }

    @Override
    public Optional<Order> findOrderById(final UUID orderId) {
        return orderRepository.findById(orderId);
    }

    @Override
    public UUID addOrder(final Order order) {
        order.setOrderStatus(OrderStatusEnum.DRAFT);
        final Order newOrder = orderRepository.save(order);
        log.debug("Created new order {}", order);
        return newOrder.getId();
    }

    @Override
    public Order updateOrderState(final UUID orderId, final OrderStatusEnum orderStatus) {
        final Optional<Order> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            final Order orderToBeUpdated = order.get();
            log.debug("Status of order {} will be updated to value {}", orderToBeUpdated, orderStatus);
            orderToBeUpdated.setOrderStatus(orderStatus);
            return orderRepository.save(orderToBeUpdated);
        } else {
            log.error("Order with id {} was not found", orderId);
            throw new NotFoundException(CommonErrorEnum.NOT_FOUND, "Order was not found");
        }
    }

    @Override
    public void deleteOrder(final UUID orderId) {
        final Optional<Order> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            final Order storedOrder = order.get();
            orderRepository.delete(storedOrder);
            log.debug("Order {} was deleted", order);
            for (OrderItem orderItem : storedOrder.getOrderItems()) {
                orderItemRepository.delete(orderItem);
                log.debug("Order item {} was deleted", orderItem);
            }
        } else {
            log.error("Order with id {} was not found", orderId);
            throw new NotFoundException(CommonErrorEnum.NOT_FOUND, "Order was not found");
        }
    }
}
