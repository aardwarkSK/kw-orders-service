package com.aardwark.kworders.service.configuration;

import com.aardwark.kworders.service.OrderService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = OrderService.class)
public class ServiceConfiguration {
}
