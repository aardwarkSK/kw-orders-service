package com.aardwark.kworders.service;

import com.aardwark.kworders.core.OrderStatusEnum;
import com.aardwark.kworders.resource.domain.Order;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderService {
    List<Order> getAllOrders();

    List<Order> getOrdersByCustomerId(UUID customerId);

    Optional<Order> findOrderById(UUID orderId);

    UUID addOrder(Order order);

    Order updateOrderState(UUID orderId, OrderStatusEnum orderStatus);

    void deleteOrder(UUID orderId);
}
